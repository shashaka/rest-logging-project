package org.blog.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class RestLoggingApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(RestLoggingApplication.class, args);
    }

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void run(String... args) throws Exception {
        restTemplate.exchange("http://google.com", HttpMethod.GET, null, String.class);
    }
}