package org.blog.test.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Collections;

@Configuration
@Slf4j
public class RestTemplateConfig {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate() {{
            setInterceptors(Collections.singletonList(clientHttpRequestInterceptor()));
        }};
    }

    private ClientHttpRequestInterceptor clientHttpRequestInterceptor() {
        return new ClientHttpRequestInterceptor() {
            @Override
            public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
                ClientHttpResponse response = execution.execute(request, body);
                doLog(request, body, response);
                return response;
            }

            private void doLog(HttpRequest request, byte[] body, ClientHttpResponse response) throws IOException {
                log.info("[Request] [{}]  {}, Headers : [{}], Body : [{}]", request.getMethod(), request.getURI(), request.getHeaders(), new String(body, Charset.forName("UTF-8")));
                log.info("[Response] [{}], Headers : [{}], Body : [{}] ", response.getStatusCode(), response.getHeaders(), new BufferedReader(new InputStreamReader(response.getBody())).readLine());
            }
        };
    }
}
